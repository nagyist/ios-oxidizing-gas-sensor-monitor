//
//  MainViewController.h
//  Oxidizing Gas Sensor Monitor
//
//  Created by Mark Rudolph on 11/14/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "BaseViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

float threshold_0[3] = {2, 1, 0.5};
float threshold_1[3] = {6, 3, 1.5};
float threshold_2[3] = {10, 5, 2.5};
float threshold_3[3] = {20, 10, 5};
float threshold_4[3] = {30, 15, 7.5};
float threshold_5[3] = {150, 75, 37.5};
float threshold_6[3] = {700, 350, 175};
float threshold_7[3] = {2000, 1000, 500};
float threshold_8[3] = {4000, 2000, 1000};




//{0, 2, 6, 10, 20, 30, 150, 700, 2000, 4000};

@interface MainViewController : BaseViewController <AVAudioPlayerDelegate>

@property NSMutableArray *baselineArray;
@property float baseline;
@property int counter;
@property NSTimer *timer;
@property int selectedSensitivity;

// Data
@property (weak, nonatomic) IBOutlet UILabel *dataDisplay;


// Buttons
@property (weak, nonatomic) IBOutlet UIButton *sensBtn_Low;
@property (weak, nonatomic) IBOutlet UIButton *sensBtn_Med;
@property (weak, nonatomic) IBOutlet UIButton *sensBtn_High;
@property (weak, nonatomic) IBOutlet UIButton *blBtn_Reset;
@property (weak, nonatomic) IBOutlet UIButton *powerBtn;

// LEDs
@property (weak, nonatomic) IBOutlet UIImageView *led_1;
@property (weak, nonatomic) IBOutlet UIImageView *led_2;
@property (weak, nonatomic) IBOutlet UIImageView *led_3;
@property (weak, nonatomic) IBOutlet UIImageView *led_4;
@property (weak, nonatomic) IBOutlet UIImageView *led_5;
@property (weak, nonatomic) IBOutlet UIImageView *led_6;
@property (weak, nonatomic) IBOutlet UIImageView *led_7;
@property (weak, nonatomic) IBOutlet UIImageView *led_8;
@property (weak, nonatomic) IBOutlet UIImageView *led_9;

// Audio
@property (nonatomic, retain) AVAudioPlayer *geiger;


- (IBAction)sensitivitySelected:(id)sender;
- (IBAction)baselineReset:(id)sender;
- (IBAction)powerToggle:(id)sender;

-(void)calculateBaseline;
-(void)setSensitivity:(int)level;
-(void)setLEDs:(int)nLit;
-(void)playGeigerTicks:(int)nTicks;

@end
