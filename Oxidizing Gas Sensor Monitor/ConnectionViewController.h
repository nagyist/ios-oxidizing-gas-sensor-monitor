//
//  ConnectionViewController.h
//  Oxidizing Gas Sensor Monitor
//
//  Created by Mark Rudolph on 11/14/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "BaseViewController.h"

@interface ConnectionViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

@property NSString *droneName;
@property (weak, nonatomic) IBOutlet UITableView *droneList;


//Buttons


- (IBAction)startScan:(id)sender;
- (IBAction)stopScan:(id)sender;
- (IBAction)disconnect:(id)sender;

@end
